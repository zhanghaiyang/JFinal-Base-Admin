# JFinal-Base-Admin

> 此项目为一个后台管理的基础框架，是从线上的项目中拆分出来。已经历过几个项目的生产环境考验。适用于中小型项目快速开发。

## 采用技术

JFinal + Shiro + FreeMarker + MySql + BootStrap 3

## 项目预览
![后台管理图片](https://gitee.com/uploads/images/2018/0208/150133_5135b32e_31.png "admin.png")
## 开发说明
 #### 1.导入 doc/jfinal-base-admin.sql
 #### 2.修改数据库配置文件

 src/main/resources/development下`example.db.properties`文件名为
 `db.properties` 并填写对应的数据库信息

 #### 3.直接add到eclipse中的tomcat,就能启动了

## 生产环境发布

#### 1. 修改`src/main/resources/production` 下的 `db.properties`为生产环境中的数据库链接

## mvn 命令打包

 ```
    mvn clean package -Pproduction -Dmaven.test.skip=true
 ```


