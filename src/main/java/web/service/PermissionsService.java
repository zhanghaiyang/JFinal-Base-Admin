package web.service;

import java.util.ArrayList;
import java.util.List;

import kit.StringUtil;
import web.model.Permissions;
import web.model.temp.VoZtree;

public class PermissionsService {
	public List<VoZtree> getVoZtreeList() {
		List<Permissions> list = Permissions.dao.find("SELECT * FROM permissions ORDER BY sort");
		List<VoZtree> znodes = new ArrayList<>();
		for (Permissions permissions : list) {
			znodes.add(new VoZtree(permissions.getId(),
					permissions.getName() + "[" + permissions.getId() + "]" + "[" + permissions.getPermission() + "]"
							+ "[" + StringUtil.defaultString(permissions.getHref(), "无") + "]",
					permissions.getParentId()));
		}
		return znodes;
	}
}
